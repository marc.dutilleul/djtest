from django.apps import AppConfig


class DroitsConfig(AppConfig):
    name = 'droits'
